#!/bin/bash

## install lunavim (lunarvim.org)
bash <(curl -s https://raw.githubusercontent.com/lunarvim/lunarvim/master/utils/installer/install.sh)
# LV_BRANCH='release-1.3/neovim-0.9' bash <(curl -s https://raw.githubusercontent.com/LunarVim/LunarVim/release-1.3/neovim-0.9/utils/installer/install.sh) # for stability


if ! command -v flatpak &>/dev/null; then
  sudo pacman --needed -S flatpak --noconfirm
fi


flatpak install flathub com.jetbrains.IntelliJ-IDEA-Ultimate
flatpak run com.jetbrains.IntelliJ-IDEA-Ultimate

flatpak install flathub com.jetbrains.WebStorm
flatpak run com.jetbrains.WebStorm

flatpak install flathub com.jetbrains.PyCharm-Professional
flatpak run com.jetbrains.PyCharm-Professional

#!/bin/bash

declare refrash_keys_cmd='pacman-key --refresh-keys '

# update
pacman -Syyu --noconfirm || {
  $refrash_keys_cmd || echo "failed to refrash keyring" >&2; exit 1
  pacman -Syyu --noconfirm
}
paru -Syyu --noconfirm


declare archlinst='./arch-pkg-lists/'


declare all=''
for f in $(find "$archlinst" -type f); do
  for p in $(grep -v "^$\|^#" "$f" | awk -F '#' '{print $1}'); do
    all="$all $p"
  done
done

echo -e "install the following packages: $all\n"

paru --needed -S $all --noconfirm

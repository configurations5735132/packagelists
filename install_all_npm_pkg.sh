#!/bin/bash

# update
npm update

declare packagelist='./pip-pkg-lists/'

declare all=''
for f in $(find "$packagelist" -type f); do
  for p in $(grep -v "^$\|^#" "$f" | awk -F '#' '{print $1}'); do
    all="$all $p"
  done
done

echo -e "install the following packages: $all\n"

npm install $all
